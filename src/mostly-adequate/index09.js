const { compose, prop, last, split, isNil } = require('ramda');
const { safeProp, join, chain, map } = require('./utils');
const Maybe = require('./utils/Maybe');
const IO = require('./utils/IO');
const { Either, Left } = require('./utils/Either');

const user = {
  id: 1,
  name: 'Albert',
  address: {
    street: {
      number: 22,
      name: 'Walnut St',
    },
  },
};

/**
 * Exercise 01
 *
 * Use `safeProp` and `map/join` or `chain` to safely get the street name when given a user
 */

// getStreetName :: User -> Maybe String
const getStreetName = compose(safeProp('name'), join, safeProp('street'), join, safeProp('address'));

console.log(getStreetName(user));
getStreetName(user).map(console.log);

/**
 * Exercise 02
 *
 * Use getFile to get the filepath, remove the directory and keep only the basename, then purely log it.
 * Hint: you may want to use `split` and `last` to obtain the basename from a filepath.
 */

// getFile :: IO String
const getFile = IO.of('./utils/index.js');

// pureLog :: String -> IO ()
const pureLog = str => new IO(() => console.log(str));

// logFilename :: IO ()
const logFilename = compose(chain(pureLog), map(last), map(split('/')));

console.log(logFilename(getFile));
logFilename(getFile).unsafePerformIO(console.log);

/**
 * Exercise 03
 *
 * Use `validateEmail`, `addToMailingList` and `emailBlast` to create a function which
 * adds a new email to the mailing list if valid, and then notify the whole list.
 */

// validateEmail :: Email -> Either String Email
const validateEmail = email => (email ? Either.of(email) : new Left('invalid email'));

// addToMailingList :: Email -> IO([Email])
const addToMailingList = email => new IO(() => [email]);

// emailBlast :: [Email] -> IO ()
const emailBlast = emails => new IO(() => console.log('sendings emails: ', emails));

// joinMailingList :: Email -> Either String (IO ())
const joinMailingList = compose(map(compose(chain(emailBlast), addToMailingList)), validateEmail);

console.log(joinMailingList());
console.log(joinMailingList('someemail'));
joinMailingList('some').map(m => m.unsafePerformIO(console.log));

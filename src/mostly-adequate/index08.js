const { add, curry } = require('ramda');
const Maybe = require('./utils/Maybe');
const { map } = require('./utils');

// incrF :: Functor f => f Int -> f Int
const incrF = map(add(1));

console.log(incrF(Maybe.of(2)));
console.log(incrF(Maybe.of(2)).$value);

const { compose, curry, prop } = require('ramda');
const Maybe = require('./Maybe');

// safeProp :: String -> Object -> Maybe a
const safeProp = curry((p, obj) => compose(Maybe.of, prop(p))(obj));

// map :: Functor f => (a -> b) -> f a -> f b
const map = curry((fn, f) => f.map(fn));

// join :: Monad m => m (m a) -> m a
const join = m => m.join();

// chain :: Monad m => (a -> m b) -> m a -> m b
const chain = curry((fn, m) => m.chain(fn));

const trace = curry((tag, x) => {
  console.log(tag, x);
  return x;
});

exports.map = map;
exports.safeProp = safeProp;
exports.join = join;
exports.chain = chain;
exports.trace = trace;

const S = require('sanctuary');
const $ = require('sanctuary-def');

const commitsCollection = {
  total_count: 4,
  incomplete_results: false,
  items: [
    {
      url: 'https://api.github.com/repos/octocat/Spoon-Knife/commits/bb4cc8d3b2e14b3af5df699876dd4ff3acd00b7f',
      sha: 'bb4cc8d3b2e14b3af5df699876dd4ff3acd00b7f',
      html_url: 'https://github.com/octocat/Spoon-Knife/commit/bb4cc8d3b2e14b3af5df699876dd4ff3acd00b7f',
      comments_url: 'https://api.github.com/repos/octocat/Spoon-Knife/commits/bb4cc8d3b2e14b3af5df699876dd4ff3acd00b7f/comments',
      commit: {
        url: 'https://api.github.com/repos/octocat/Spoon-Knife/git/commits/bb4cc8d3b2e14b3af5df699876dd4ff3acd00b7f',
        author: {
          date: '2014-02-04T14:38:36-08:00',
          name: 'The Octocat',
          email: 'octocat@nowhere.com',
        },
        committer: {
          date: '2014-02-12T15:18:55-08:00',
          name: 'The Octocat',
          email: 'octocat@nowhere.com',
        },
        message: 'Create styles.css and updated README',
        tree: {
          url: 'https://api.github.com/repos/octocat/Spoon-Knife/git/trees/a639e96f9038797fba6e0469f94a4b0cc459fa68',
          sha: 'a639e96f9038797fba6e0469f94a4b0cc459fa68',
        },
        comment_count: 8,
      },
      score: 4.9971514,
    },
    {
      url: 'https://api.github.com/repos/octocat/Spoon-Knife/commits/a639e96f9038797fba6e0469f94a4b0cc459fa6844',
      sha: 'a639e96f9038797fba6e0469f94a4b0cc459fa6844',
      html_url: 'https://github.com/octocat/Spoon-Knife/commit/a639e96f9038797fba6e0469f94a4b0cc459fa6844',
      comments_url: 'https://api.github.com/repos/octocat/Spoon-Knife/commits/a639e96f9038797fba6e0469f94a4b0cc459fa6844/comments',
      commit: {
        url: 'https://api.github.com/repos/octocat/Spoon-Knife/git/commits/a639e96f9038797fba6e0469f94a4b0cc459fa6844',
        author: {
          date: '2014-02-04T14:38:36-08:00',
          name: 'The Octocat',
          email: 'octocat@nowhere.com',
        },
        committer: {
          date: '2014-02-12T15:18:55-08:00',
          name: 'The Octocat',
          email: 'octocat@nowhere.com',
        },
        message: 'Updated eslint configuration',
        tree: {
          url: 'https://api.github.com/repos/octocat/Spoon-Knife/git/trees/a639e96f9038797fba6e0469f94a4b0cc459fa6844',
          sha: 'a639e96f9038797fba6e0469f94a4b0cc459fa6844',
        },
        comment_count: 8,
      },
      score: 4.9971514,
    },
    {
      url: 'https://api.github.com/repos/octocat/Spoon-Knife/commits/a639e96f9038797fba6e0469f94a4b0cc459fa6844',
      sha: 'a639e96f9038797fba6e0469f94a4b0cc459fa6844',
      html_url: 'https://github.com/octocat/Spoon-Knife/commit/a639e96f9038797fba6e0469f94a4b0cc459fa6844',
      comments_url: 'https://api.github.com/repos/octocat/Spoon-Knife/commits/a639e96f9038797fba6e0469f94a4b0cc459fa6844/comments',
      commit: {
        url: 'https://api.github.com/repos/octocat/Spoon-Knife/git/commits/a639e96f9038797fba6e0469f94a4b0cc459fa6844',
        author: {
          date: '2014-02-04T14:38:36-08:00',
          name: 'The Octocat',
          email: 'octocat@nowhere.com',
        },
        committer: {
          date: '2014-02-12T15:18:55-08:00',
          name: 'The Octocat',
          email: 'octocat@nowhere.com',
        },
        message: null,
        tree: {
          url: 'https://api.github.com/repos/octocat/Spoon-Knife/git/trees/a639e96f9038797fba6e0469f94a4b0cc459fa6844',
          sha: 'a639e96f9038797fba6e0469f94a4b0cc459fa6844',
        },
        comment_count: 8,
      },
      score: 4.9971514,
    },
    {
      url: 'https://api.github.com/repos/octocat/Spoon-Knife/commits/bb4cc8d3b2e14b3af5df699876dd4ff3acd00b7f3333',
      sha: 'bb4cc8d3b2e14b3af5df699876dd4ff3acd00b7f3333',
      html_url: 'https://github.com/octocat/Spoon-Knife/commit/bb4cc8d3b2e14b3af5df699876dd4ff3acd00b7f3333',
      comments_url: 'https://api.github.com/repos/octocat/Spoon-Knife/commits/bb4cc8d3b2e14b3af5df699876dd4ff3acd00b7f3333/comments',
      commit: {
        url: 'https://api.github.com/repos/octocat/Spoon-Knife/git/commits/bb4cc8d3b2e14b3af5df699876dd4ff3acd00b7f33333',
        author: {
          date: '2014-02-04T14:38:36-08:00',
          name: 'The Octocat Clone',
          email: 'octocat-clone@nowhere.com',
        },
        committer: {
          date: '2014-02-12T15:18:55-08:00',
          name: 'The Octocat Clone',
          email: 'octocat-clone@nowhere.com',
        },
        message: 'Create styles.css and updated README',
        tree: {
          url: 'https://api.github.com/repos/octocat/Spoon-Knife/git/trees/bb4cc8d3b2e14b3af5df699876dd4ff3acd00b7f33333',
          sha: 'bb4cc8d3b2e14b3af5df699876dd4ff3acd00b7f33333',
        },
        comment_count: 8,
      },
      score: 4.9971514,
    },
  ],
};

const committerIs = committer => {
  return _ => S.pipe([S.gets(S.is($.String))(['commit', 'committer', 'email']), S.equals(S.Just(committer))])(_);
};

const getCommitsMessagesByUser = (commits, user) => {
  return S.pipe([
    S.get(S.is($.Array($.Object)))('items'),
    S.map(S.filter(committerIs(user))),
    S.map(S.map(S.gets(S.is($.String))(['commit', 'message']))),
    S.map(S.justs),
    S.fromMaybe([]),
  ])(commits);
};

const x = getCommitsMessagesByUser(commitsCollection, 'octocat@nowhere.com');

console.log(x);

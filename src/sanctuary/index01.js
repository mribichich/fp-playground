const S = require('sanctuary');
const $ = require('sanctuary-def');

//
// parseInt :: Radix -> String -> Maybe Integer

console.log(S.parseInt(10)('-42'));
// Just (-42)

console.log(S.parseInt(16)('0xFF'));
// Just (255)

console.log(S.parseInt(16)('0xGG'));
// Nothing

console.log(S.maybeToNullable(S.parseInt(10)('-42')));
// -42

console.log(S.maybeToNullable(S.parseInt(10)('xxx')));
// -42

console.log(S.maybe(0)(S.prop('length'))(S.Just('refuge')));

//
// gets :: (Any -> Boolean) -> Array String -> a -> Maybe b

const foo = {
  bar: {
    thing: {
      exists: true,
    },
  },
};

console.log(S.gets(S.is($.Boolean))(['bar', 'thing', 'exists'])(foo));
// Just(true)

console.log(S.gets(S.is($.Number))(['bar', 'thing', 'exists'])(foo));
// Nothing

console.log(S.gets(S.is($.Number))(['data', 'user', 'name'])({}));
// Nothing
